# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import time, sys

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

r = 26
g = 24
b = 22

GPIO.cleanup([r,g,b])
GPIO.setup([r,g,b], GPIO.OUT)

def switch(n):
	if n == "1":
		mode_1()
	if n == "2":
		mode_2()
	if n == "3":
		mode_3()

def light(pin, tm):
	GPIO.output(pin, True)
	time.sleep(tm)
	GPIO.output(pin, False)
	time.sleep(tm)

def mode_1():
	tm = 1
	while True:
		light(r, tm)
		light(g, tm)
		light(b, tm)

def mode_2():
	while True:
		GPIO.output(r, True)
		time.sleep(2)
		GPIO.output(g, True)
		time.sleep(2)
		GPIO.output(b, True)
		time.sleep(5)
		GPIO.output([r,g,b],False)
		time.sleep(2)

def mode_3():
	tm = 0.2
	count = 1
	while True:
		light(r, tm)
		light(g, tm)
		light(b, tm)

		if count%3 == 0:
			GPIO.output([r,g,b], True)
			time.sleep(5)
			GPIO.output([r,g,b],False)
			time.sleep(2)
		
		count+=1



if __name__ == "__main__":
	try:
		mode = sys.argv[1]
		switch(mode)
	except:
		pass
	finally:
		GPIO.cleanup([r,g,b])