import RPi.GPIO as GPIO
import sys, time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
led = 37

morse = sys.argv[1]
GPIO.setup(led, GPIO.OUT)
puls = GPIO.PWM(led, float(0.7)) # nwm proc
puls.start(0) 
try:
	while True:
		#puls.start(0)
		for c in morse:
			print(c)
			if c == '.':
				puls.ChangeDutyCycle(75.0)
			if c == '-':
				puls.ChangeDutyCycle(25.0)
			if c == '/':
				puls.ChangeDutyCycle(100.0)
			time.sleep(float(1/0.7))

		puls.stop()
		time.sleep(2)
finally:
	puls.stop()
	GPIO.cleanup()
