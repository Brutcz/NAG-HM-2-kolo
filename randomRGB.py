import RPi.GPIO as GPIO
import time
import random

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

r = 40
g = 38
b = 36

GPIO.setup([r,g,b],GPIO.OUT)

rp = GPIO.PWM(r, 80)
gp = GPIO.PWM(g, 80)
bp = GPIO.PWM(b, 80)

rp.start(0)
gp.start(0)
bp.start(0)

try:
	while True:
		#time.sleep(0.5)
		grc = random.uniform(0,100)
		brc = random.uniform(0,100)
		rrc = random.uniform(0,100)
		rp.ChangeDutyCycle(rrc)
		gp.ChangeDutyCycle(grc)
		bp.ChangeDutyCycle(brc)
		time.sleep(1)
except:
	pass
finally:
	GPIO.cleanup([r,g,b])
