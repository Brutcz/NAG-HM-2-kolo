import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

led = 31
power = 33
status = 1

GPIO.setup(led, GPIO.OUT)
GPIO.output(led, True)
GPIO.setup(power, GPIO.IN, GPIO.PUD_DOWN)
try:
	while time.time()+0.2 > time.time():
		
		if int(GPIO.input(power)) == 1:
			if status == 0:
				GPIO.output(led, False)
				status = 1
			else:
				GPIO.output(led, True)
				status = 0
except:
	pass
finally:
	GPIO.cleanup([led,power])
