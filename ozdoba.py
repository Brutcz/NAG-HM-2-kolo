# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import time, sys, subprocess

GPIO.setmode(GPIO.BOARD)

btn = 33
btn_press = 0	# hodnoty 1-3 aneb číslo spuštěného programu 

GPIO.setup(btn, GPIO.IN, GPIO.PUD_DOWN)

if __name__ == "__main__":

	try:
		p = None
		while True:
			while time.time()+0.2 > time.time():
				if int(GPIO.input(btn)) == 1:
					print("Pressed")
					if btn_press == 3:
						btn_press = 1
					else:
						btn_press+=1
					break
			time.sleep(1)
			if p:
				p.terminate()
			print(btn_press)
			p = subprocess.Popen(["python","./ozdoba_sub/mode1.py", str(btn_press)], shell=False)
	
	except Exception as e:
		#e = sys.exc_info()[0]
		print("Chyba")
		print(e)
	finally:
		GPIO.cleanup([btn])