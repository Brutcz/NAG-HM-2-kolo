
import RPi.GPIO as GPIO
import time
import random


GPIO.setmode(GPIO.BOARD)
#GPIO.setwarnings(False)

player1 = 0
player2 = 0

led = 18
btn1 = 21
btn2 = 23

GPIO.setup(led, GPIO.OUT)
GPIO.setup(btn1, GPIO.IN, GPIO.PUD_DOWN)
GPIO.setup(btn2, GPIO.IN, GPIO.PUD_DOWN)

try:
	while True:
		time.sleep(0.3)
	
		rand = random.uniform(3,9)
		#print(rand)
		myTime = time.time() + rand
                while myTime > time.time():
                        if int(GPIO.input(btn1)) == 1:
                                player1-=1
                                time.sleep(0.2)

                        if int(GPIO.input(btn2)) == 1:
                                player2-=1
                                time.sleep(0.2)


		GPIO.output(led, True)
		
		myTime = time.time() + 0.75
		while myTime > time.time():
			if int(GPIO.input(btn1)) == 1:
				player1+=1
				break

			if int(GPIO.input(btn2)) == 1:
				player2+=1
				break
	
		#time.sleep(0.75)
		print("\nSkore %d:%d"%(player1,player2))
		GPIO.output(led, False)
finally:
	print("Konec")
	GPIO.cleanup([led,btn1,btn2])
